'use strict';

// Include the excellent XML-Splitter node module
var XMLSplitter = require('xml-splitter');
var path = require('path');


/**
 * Main bootstrap
 */
function init() {
  var inputFile;
  var rootItem;
  var splitCount;
  checkArguments();


  /**
   * Ensure arguments are valid
   */
  function checkArguments() {
    if (!process.argv[1] || (inputFile != path.normalize(process.argv[1]))) {
      console.error('No valid input XML specified. Usage: $ node ./xml-file-splitter.js /path/to/input.xml /root/item NUMSLICES');
      process.exit(1);
    }
    else {

    }
    if (!process.argv[2]) {
      console.error('No root XML selected to split on. Usage: $ node ./xml-file-splitter.js /path/to/input.xml /root/item NUMSLICES');
      process.exit(1);
    }
    else {
      rootItem = process.argv[2];
    }
    if (!process.argv[3] || isNaN(process.argv[3])) {
      console.error('Must specifier the number of slices to output. Usage: $ node ./xml-file-splitter.js /path/to/input.xml /root/item NUMSLICES');
      process.exit(1);
    }
    else {
      splitCount = process.argv[3];
    }
  }

  /**
   *
   */

}




// Start bootstrap
init();